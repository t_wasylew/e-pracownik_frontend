'use strict';

angular.module('myApp.viewWorkers', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewWorkers', {
            templateUrl: 'viewWorkers/viewWorkers.html',
            controller: 'ViewWorkersCtrl'
        });
    }])

    .controller('ViewWorkersCtrl', ['$http', 'authService', function ($http, authService) {
        var self = this;
        var URL = 'http://localhost:8080/company';
        this.workersList = [];
        this.currentPage = 0;
        this.totalPages = 0;
        this.companyID = authService.loggedInUser.id;

        this.removeWorker ={
            'companyID': companyID,
        };

        this.redirectToAccount = function (accountId) {
            window.location = "#!/viewWorkerAccount?workerId="+accountId;
        };

        this.removeWorkers = function (id_remove) {
            self.removeWorker.userID = id_remove;
            console.log(self.removeWorker);
            $http.post(URL+'/deleteWorkerWithID', self.removeWorker)
                .then(function (resp) {
                    window.location.reload();
                    console.log("Delete Success");
                }, function (resp) {
                    console.log("Delete Error: " + resp);
                })
        };

        this.fetchWorkersList = function (page_number) {
            $http.get(URL+'/page?companyID='+self.companyID+'&page='+page_number)
                .then(
                    function (data) {
                        console.log(data);
                        var users = data.data.objects;

                        // czyszczenie kolekcji ze starych wpisów
                        self.workersList = [];

                        for (var index in users) {
                            console.log(users[index]);
                            self.workersList.push(users[index]);
                        }
                        self.currentPage = data.data.currentPage;
                        self.totalPages = data.data.totalPages;
                    },
                    function () {
                        console.log("error");
                    }
                );
        };

        this.fetchNext = function () {
            if (self.currentPage < self.totalPages) {
                self.fetchWorkersList(self.currentPage + 1);
            }
        };

        this.fetchPrevious = function () {
            if (self.currentPage == 0) {
                return;
            }
            self.fetchWorkersList(self.currentPage - 1);
        };

        self.fetchWorkersList(self.currentPage);
    }]);
