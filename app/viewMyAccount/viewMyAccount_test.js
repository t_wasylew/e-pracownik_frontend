'use strict';

describe('myApp.viewMyAccount module', function() {

  beforeEach(module('myApp.viewMyAccount'));

  describe('viewMyAccount controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var viewMyAccountCtrl = $controller('ViewMyAccountCtrl');
      expect(viewMyAccountCtrl).toBeDefined();
    }));

  });
});