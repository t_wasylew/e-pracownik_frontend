'use strict';

angular.module('myApp.viewMyAccount', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewMyAccount', {
            templateUrl: 'viewMyAccount/viewMyAccount.html',
            controller: 'ViewMyAccountCtrl'
        });
    }])

    .controller('ViewMyAccountCtrl', ['$http', 'authService', 'loggedAccountService', function ($http, authService, loggedAccountService) {

        var self = this;
        this.companyAccount = "unknown";

        this.fetchCompanyAccount = function () {
            loggedAccountService.fetchUser()
                .then(function (data) {
                    self.companyAccount = data;
                }, function () {
                console.log("Fetch company account error")
                });
        };
        this.fetchCompanyAccount();
    }]);