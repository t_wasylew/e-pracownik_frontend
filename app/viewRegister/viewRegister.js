'use strict';

angular.module('myApp.viewRegister', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewRegister', {
            templateUrl: 'viewRegister/viewRegister.html',
            controller: 'ViewRegisterCtrl'
        });
    }])

    .controller('ViewRegisterCtrl', ['$http', function ($http) {
        var self = this;
        var URL = 'http://localhost:8080';
        this.formUser = {
            'companyName': '',
            'login': '',
            'password': '',
            'email': ''
        };
        this.sendToBackend = function () {
            $http.post(URL + '/registerCompanyAccount', self.formUser)
                .then(function (data) {
                    console.log(data);
                    window.location = "#!/viewSign";
                }, function (data) {
                    console.log("error");

                })
        };
    }]);