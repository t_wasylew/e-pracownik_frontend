'use strict';

angular.module('myApp.viewAddWorker', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewAddWorker', {
            templateUrl: 'viewAddWorker/viewAddWorker.html',
            controller: 'ViewAddWorkerCtrl'
        });
    }])

    .controller('ViewAddWorkerCtrl', ['$http', 'authService', function ($http, authService) {
        var self = this;
        var URL = 'http://localhost:8080/company';
        var companyID = authService.loggedInUser.id;

        this.formWorker = {
            'login': '',
            'password': '',
            'email': '',
            'companyID': companyID
        };

        this.addWorker = {
            'companyID': companyID,
        };

        this.sendToBackend = function () {
            $http.post(URL + '/registerWorker', self.formWorker)
                .then(function (data) {
                    console.log(data);
                    self.addWorker.userID = data.data;
                    console.log(self.addWorker);
                    $http.post(URL+'/addNewWorker', self.addWorker)
                        .then(function (data) {
                            console.log('succes'+ data)
                            }, function () {
                            console.log("Adding error");
                            }
                        );

                    window.location = "#!/viewMyAccount";
                }, function (data) {
                    console.log("Registration error");

                })
        };
    }]);