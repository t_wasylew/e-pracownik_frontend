'use strict';

angular.module('myApp.passDataService', ['ngRoute']).service('passDataService', ['$http', '$rootScope', function ($http, $rootScope) {

    var savedData = {}
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }

}]);