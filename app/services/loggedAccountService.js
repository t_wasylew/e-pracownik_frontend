'use strict';

angular.module('myApp.loggedAccountService', ['ngRoute']).service('loggedAccountService', ['$http', 'authService', '$q', function ($http, authService, $q) {


    this.fetchUser = function () {
        var deferred = $q.defer();
        $http.get('http://localhost:8080/getUserByID?id=' + authService.logged_in_id)
            .then(
                function (data) {
                    return deferred.resolve(data.data);
                }, function (data) {
                    deferred.reject(data);
                });
        return deferred.promise;
    };
}]);
