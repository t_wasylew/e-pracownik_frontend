'use strict';

describe('myApp.viewWorkerAccount module', function() {

  beforeEach(module('myApp.viewWorkerAccount'));

  describe('viewWorkerAccount controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var viewWorkerAccountCtrl = $controller('ViewWorkerAccountCtrl');
      expect(viewWorkerAccountCtrl).toBeDefined();
    }));

  });
});