'use strict';

angular.module('myApp.viewWorkerAccount', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewWorkerAccount', {
            templateUrl: 'viewWorkerAccount/viewWorkerAccount.html',
            controller: 'ViewWorkerAccountCtrl'
        });
    }])

    .controller('ViewWorkerAccountCtrl', ['$http', 'authService', '$routeParams', function ($http,authService, $routeParams) {
        var URL = 'http://localhost:8080';
        var self = this;

        this.currentWorkerAccount = $routeParams.workerID;

        console.log("Redirected with = " + this.currentWorkerAccount);
    }]);