'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.view1',
    'myApp.viewRegister',
    'myApp.viewWorkers',
    'myApp.viewHome',
    'myApp.viewSign',
    'myApp.viewMyAccount',
    'myApp.viewAddWorker',
    'myApp.viewWorkerAccount',
    'myApp.loggedAccountService',
    'myApp.passDataService',
    'myApp.authService',
    'myApp.version'
]).config(['$locationProvider', '$routeProvider',
    function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.otherwise({redirectTo: '/viewHome'});

    }]).run(function ($rootScope, authService, $window, $http) {

    if (authService.logged_in_id === '') { // nie jest zalogowany
        var token = $window.sessionStorage.getItem('token');
        var user_id = $window.sessionStorage.getItem('user_id');

        if (token !== null) {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            authService.logged_in_id = user_id;
            $rootScope.loggedIn = true;
        }else{
            $rootScope.loggedIn = false;
        }
    } else {
        $rootScope.loggedIn = true;
    }
});