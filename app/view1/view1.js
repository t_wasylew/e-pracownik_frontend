'use strict';

angular.module('myApp.view1', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])

    .controller('View1Ctrl', ['$http', 'authService', function ($http, authService) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.userList = [];
        this.currentPage = 0;
        this.totalPages = 0;

        this.loggedInUser = authService.logged_in_id;

        this.fetchUsers = function (page_number) {
            $http.get(URL + '/page?page=' + page_number)
                .then(
                    function (data) {
                        console.log(data);
                        var users = data.data.objects;

                        // czyszczenie kolekcji ze starych wpisów
                        self.userList = [];
                        for (var index in users) {
                            console.log(users[index]);
                            self.userList.push(users[index]);
                        }
                        self.currentPage = data.data.currentPage;
                        self.totalPages = data.data.totalPages;
                    },
                    function () {
                        console.log("error");
                    }
                );
        };

        this.fetchNext = function () {
            if (self.currentPage < self.totalPages) {
                self.fetchUsers(self.currentPage + 1);
            }
        };

        this.fetchPrevious = function () {
            if (self.currentPage == 0) {
                return;
            }
            self.fetchUsers(self.currentPage - 1);
        };

        self.fetchUsers(self.currentPage);
    }]);