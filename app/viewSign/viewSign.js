'use strict';

angular.module('myApp.viewSign', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewSign', {
            templateUrl: 'viewSign/viewSign.html',
            controller: 'ViewSignCtrl'
        });
    }])

    .controller('ViewSignCtrl', ['$http', 'authService', '$window', '$rootScope', function ($http, authService, $window, $rootScope) {
        var self = this;
        var URL = 'http://localhost:8080';

        this.formUser = {
            'login': '',
            'password': ''
        };

        this.authenticate = function () {
            $http.post(URL + "/authenticate", self.formUser)
                .then(function (resp) {
                        console.log("Success" + resp);
                        var token = resp.data.token;
                        var loggedInUser = resp.data.appUser;

                        console.log(resp.data.token);
                        console.log(resp.data.appUser);
                        authService.loggedInUser = loggedInUser;
                        authService.logged_in_id = loggedInUser.id;


                        $window.sessionStorage.setItem('token', token);
                        $window.sessionStorage.setItem('user_id', loggedInUser.id);

                        $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                        window.location = '#!/viewHome';
                        $rootScope.loggedIn = true;
                    },
                    function (resp) {
                        console.log("Error " + resp);
                    });
        };
    }]);